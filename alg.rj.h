#ifndef __ALG_RJ_H__
#define __ALG_RJ_H__

#include "math.rj.h"
#include "debug.rj.h"
#include <cstdlib>

template <typename T>
void sort_rj(T* a,int* m,int l,int r,bool (*cmp)(const T& x, const T& y)){
	int i = l,j=r;
	rjEle("i",i);
	rjEle("j",j);
	T mid ;
	mid = a[m[(i*13+j*17)/30]];
	while (i<=j){
		rj(i,j);
		while (cmp(a[m[i]],mid))i++;
		while (cmp(mid,a[m[j]]))j--;
		if (i<=j)
			swap_rj(m[i++],m[j--]);
		rj(i,j);
	}
	if(i<r)sort_rj(a,m,i,r,cmp);
	if(l<j)sort_rj(a,m,l,j,cmp);
}

template<typename T>
void selectK(T* a, int n,int k){
	for(int lo = 0,hi = n-1;lo<hi;){
		swap_rj(a[lo],a[lo+rand()%(hi-lo+1)]);
		int i = lo,j = hi;
		T pivot = a[i];
		while (i<j){
			while((i<j)&&(pivot<a[j]))j--;a[i]=a[j];
			while((i<j)&&(a[i]<pivot))i++;a[j]=a[i];
		}
		a[i]=pivot;
		if(k<=i)hi=i-1;
		if(i<=k)lo=i+1;
	}
}
template<typename T>
void selectKforKDTREE(T* a, int n,int k,int dim){
	for(int lo = 0,hi = n-1;lo<hi;){
		swap_rj(a[lo],a[lo+rand()%(hi-lo+1)]);
		int i = lo,j = hi;
		T pivot = a[i];
		while (i<j){
			while((i<j)&&(pivot[dim]<=a[j][dim]))j--;a[i]=a[j];
			while((i<j)&&(a[i][dim]<=pivot[dim]))i++;a[j]=a[i];
		}
		a[i]=pivot;
		if(k<=i)hi=i-1;
		if(i<=k)lo=i+1;
	}
}

#endif
