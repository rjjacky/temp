#ifndef __HASHTABLE_RJ_H__
#define __HASHTABLE_RJ_H__

#include "cachecontroler.h"
#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

static const int __stl_num_primes = 28;
static const unsigned long __stl_prime_list[__stl_num_primes] = {
	53,			97,			193,			389,			769,
	1543,		3079,		6151,			12289,			24593,
	49157,		98317,		196613,			393241,			786433,
	1572869,	3145739,	6291469,		12582917,		25165843,
	50331653,	100663319,	201326611,		402653189,		805306457,
	1610612741,	3221225473ul,4294967291ul
};

// BKDR Hash Function
inline unsigned int BKDRHash(const string& str) {
	unsigned int seed = 131; // 31 131 1313 13131 131313 etc..
	unsigned int hash = 0;
 
	int l = str.length();
	for(int i = 0 ;i < l ;i ++){
		hash = hash * seed + str[i];
	}
 
	return (hash & 0x7FFFFFFF);
} 
// RBKDR Hash Function
inline unsigned int rBKDRHash(const string& str) {
	unsigned int seed = 131; // 31 131 1313 13131 131313 etc..
	unsigned int hash = 0;
 
	int l = str.length();
	for(int i = l-1 ;i >=0 ;i --){
		hash = hash * seed + str[i];
	}
 
	return (hash & 0x7FFFFFFF);
} 

// AP Hash Function
inline unsigned int APHash(const string& str)
{
	unsigned int hash = 0;
	int i;
	int l = str.length();
	for (i=0; i<l; i++) {
		if ((i & 1) == 0)
		{
			hash ^= ((hash << 7) ^ str[i] ^ (hash >> 3));
		}
		else
		{
			hash ^= (~((hash << 11) ^ str[i] ^ (hash >> 5)));
		}
	}
 
	return (hash & 0x7FFFFFFF);
}
inline unsigned int Hash2(const string& str){
	return APHash(str);
//	return rBKDRHash(str);
}



template<typename valueT>
class hashNode{
public:
	valueT value;
	int hashValue;
	hashNode* next;
	hashNode(){
		next = NULL;
	}
};

template<typename keyT,typename valueT>
class hashTable{

	valueT nullValue;
	int _capacity;
	cacheControler<hashNode<valueT> > *hashNodeControler;
	hashNode<valueT> ** buckets;

public:
	hashTable(int maxUnits, valueT _nullValue){
		nullValue = _nullValue;
		for(int i = 0 ;i <__stl_num_primes;i++){
			if(__stl_prime_list[i]>=maxUnits){
				_capacity = __stl_prime_list[i];
				break;
			}
		}
		hashNodeControler = new cacheControler<hashNode<valueT> >(maxUnits);
		buckets = new hashNode<valueT>*[_capacity];
		for (int i = 0;i<_capacity;i++){
			buckets[i] = NULL;
		}

	}

	~hashTable(){
		if(hashNodeControler)
			delete hashNodeControler;
		if(buckets)
			delete[] buckets;
	}

	void insert(keyT key,valueT value){
		int hv1 = BKDRHash(key)%_capacity;
		hashNode<valueT>* cur = hashNodeControler->request();
		cur->next = buckets[hv1];
		buckets[hv1] = cur;
		cur->hashValue = Hash2(key);
		cur->value = value;
	}

	int operator[] (keyT key){
		int hv1 = BKDRHash(key)%_capacity;
		int hv2 = Hash2(key);
		for(hashNode<valueT>* e = buckets[hv1];e;e=e->next){
			if (e->hashValue == hv2)
				return e->value;
		}
		return nullValue;
	}

};






#endif
