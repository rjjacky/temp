#ifndef __IO_RJ_H__
#define __IO_RJ_H__

#include <cstdio>
#include <cstring>
using namespace std;



bool strValidator[128];
void clearStrValidator(){
	memset(strValidator,0,sizeof(strValidator));
}
void setStrValidator(char st,char ed){
	for(int i = st;i<=ed;i++){
		strValidator[i] = true;
	}
}
void getStr(char* buffer){
	char c; 
	char* ret=buffer;
	while (strValidator[c = getchar()]){
		*(ret++) = c;
	}
	*ret = 0;
}

		


/****读入正负整数****/
int getint()
{
	char ch;
	while((ch=getchar()))
		if (('0'<=ch&&ch<='9')||ch=='-')break;
	bool f=0;
	if(ch=='-')
	{f=1;ch=getchar();}
	int t=0;for (;'0'<=ch&&ch<='9';ch=getchar())t=t*10+ch-'0';
	return f?-t:t;
}
/****读入非负整数****/ 
inline int getPInt(char& ch)
{
	while ((ch=getchar()))if('0'<=ch&&ch<='9')break;
	int t=0;for (;'0'<=ch&&ch<='9';ch=getchar())t=t*10+ch-'0';
	return t;
} 
inline int getPInt()
{
	char ch;
	while ((ch=getchar()))if('0'<=ch&&ch<='9')break;
	int t=0;for (;'0'<=ch&&ch<='9';ch=getchar())t=t*10+ch-'0';
	return t;
} 
/****读入实数****/
double getdouble()
{
	double t=0,l=10;
	bool f=0;
	char ch;
	for (ch=getchar();;ch=getchar())
		if (('0'<=ch&&ch<='9')||ch=='-')break;
	if(ch=='-'){f=1;ch=getchar();}
	for (;'0'<=ch&&ch<='9';ch=getchar())t=t*10+ch-'0';
	if(ch=='.')for(ch=getchar();'0'<=ch&&ch<='9';ch=getchar(),l*=10)t+=(ch-'0')/l;
	if(f)return -t;else return t;
}
int s0,s[20];
void Putint(int x)
{
	if(x==0){putchar('0');return;}
	s0=0;
	if(x<0){x=-x;putchar('-');}
	while (x){s[++s0]=x%10+'0';x/=10;}
	while(s0)putchar(s[s0--]);
}

void outStr(char* p){
	while(*p){
		putchar(*p);
		p++;
	}
	putchar('\n');
}
#endif
