
#include <iostream>
#define rep(i,n) for (int i=0;i<(n);++i)
#define ft(i,a,b) for (int i=(a);i<=(b);++i)
#define fdt(i,a,b) for (int i=(a);i>=b;--i)

#define DEFAULT_CAPACITY 3
#define pub push_back
#define pob pop_back

template <typename T>
class vec{
private:
    int _size;
    int _capacity;
    T* _elem;

protected:
    void expand(){
        if ( _size < _capacity ) return;
        if ( _capacity < DEFAULT_CAPACITY) _capacity = DEFAULT_CAPACITY;
        T* oldElem = _elem;
        _elem = new T[_capacity<<=1];
        for (int i = 0;i<_size;i++)
            _elem[i] = oldElem[i];
        delete[] oldElem;
    }

    void quickSort(int l,int r){
        T mid = _elem[(l+r)>>1];
        int i = l , j = r;
        while(i<=j){
            while (_elem[i]<mid)i++;
            while (_elem[j]>mid)j--;
            if (i<=j)
                swap(_elem[i++],_elem[j--]);
        }
        if(i<r)quickSort(i,r);
        if(l<j)quickSort(l,j);
    }

    void copyFrom(T* const A,int lo,int hi){
        //if(_elem)delete[] _elem;
        _elem = new T[_capacity=2*(hi-lo)];
        _size = 0;
        while (lo<hi)
            _elem[_size++] = A[lo++];
    }
public:
    vec(int c = DEFAULT_CAPACITY){
        _elem = new T[_capacity = c];
        _size = 0;
    }
	vec(vec<T>& data){
		copyFrom(data._elem,0,data._size);
	}
    ~vec(){
        delete [] _elem;
    }

    int size()const{return _size;}

    T& operator[](int r)const{
		if (r>=_size||r<0){
			std::cerr<<"invalid index\n";
			
		}
        return _elem[r];
    }
    vec<T>& operator= (vec<T> const& V){
        if(_elem) delete[] _elem;
        copyFrom(V._elem,0,V.size());
        return *this;
    }
    bool operator== (vec<T> const& V){
        rep(i,_size){
            if(_elem[i]!=V[i])
                return false;
        }
        return true;
    }

    void push_back(T const& e){
        expand();

        _elem[_size++] = e;
    }
    void pop_back(){
        if(_size)
            _size--;
    }

    void sort(){
        quickSort(0,_size-1);
    }

    T& back()const{
        return _elem[_size-1];
    }

    template <typename T2>
    int search_left(T2 const& key)const{
        int l = 0,r = _size;
        T mid;
        while (l<r){
            mid = (l+r) >> 1;
            (key < _elem[mid])? r = mid:l = mid+1;
        }
        return --l;
    }
    template <typename T2>
    int search_right(T2 const& key)const{
        int l = 0,r = _size;
        int mid;
        while (l<r){
            mid = (l+r)>>1;
            (key <= _elem[mid])? r = mid:l = mid+1;
        }
        return r;
    }

    void reverse(){
        int l = 0,r = _size;
        while(l<r)
            swap(_elem[l++],_elem[--r]);
    }
    vec<T> getReverse(){
        vec<T> tmp(_capacity);
        fdt(i,_size-1,0)
            tmp.push_back(_elem[i]);
        return tmp;
    }
    void clear(){
        _size = 0;
    }

};
