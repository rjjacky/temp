/**
 * 默认需要node提供getkey()
 *
 **/
#include <vector>
template < typename keyType,typename nodeType>
struct heapNode{
	keyType key;
	nodeType* nodePointer;
	heapNode(keyType k,nodeType* n){
		key = k;
		nodePointer = n;
	}
};
// 堆类
template <typename keyType,typename nodeType>
class heap{
protected:
	vector< heapNode<keyType,nodeType> > data;
	void heapup(int x){
		while ( x>1 && data[x].key < data[x>>1].key ){
			swap(data[x],data[x>>1]);
			swap(data[x].nodePointer->pos,data[x>>1].nodePointer->pos);
			x >>= 1;
		}
	}
	void heapdown(int i){
		int j , n = data.size()-1;
		while ( (j=i<<1)<=n ){
			if (j<n&&data[j].key > data[j+1].key) j ++ ;
			if (data[i].key <= data[j].key)
				break;
			swap(data[i],data[j]);
			swap(data[i].nodePointer->pos,data[j].nodePointer->pos);
			i = j;
		}
	}
			
public:
	heap(){
		data.push_back(heapNode<keyType,nodeType>(0,NULL));
	}
	void insert(nodeType* d){
		heapNode<keyType,nodeType> tmp(d->getKey(),d);
		data.push_back(tmp);
		heapup(data.size()-1);
	}
	nodeType* top(){
		return data[1].nodePointer;
	}
	void pop(){
		swap(data[1],data.back());
		data.pop_back();
		heapdown(1);
	}
	bool empty(){
		return data.size()==1;
	}
	int size(){
		return data.size()-1;
	}
	void maintain(int p){
		heapdown(p);
		heapup(p);
	}
};


