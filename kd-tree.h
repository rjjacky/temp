#ifndef __KDTREE_H__
#define __KDTREE_H__

#include "cacheControler.h"
#include "alg.rj.h"
#include <iostream>
#include <assert.h>
using namespace std;
typedef long long LL;
const int dimension = 2;

enum regionRelation{
	APART = 0 , INCLUDED, INTERSECTED
};



template< typename nodeType, typename keyType>
struct kdTreeNode{
	kdTreeNode *left,*right;
	keyType maxKey[dimension],minKey[dimension];
	nodeType* data;
	int nodesCount;
	LL sum;

	void trash(cacheControler<kdTreeNode<nodeType,keyType> >* nodeControler){
		if(left){
			left->trash(nodeControler);
		}
		if (right){
			right->trash(nodeControler);
		}
		nodeControler->trash(this);
	}

	void init(kdTreeNode* l,kdTreeNode* r,nodeType* nodeData,int nc = 1){
		data = nodeData;
		left = l;
		right = r;
		this->nodesCount =nc;
		if(data){
			switch(dimension){
				case 3:
					maxKey[2] = minKey[2] = data->keys[2];
				case 2:
					maxKey[1] = minKey[1] = data->keys[1];
				case 1:
					maxKey[0] = minKey[0] = data->keys[0];
			}
			sum = data->value;
		}
		else{
			switch(dimension){
				case 3:
					maxKey[2] = intInf; minKey[2] = -intInf;
				case 2:
					maxKey[1] = intInf; minKey[1] = -intInf;
				case 1:
					maxKey[0] = intInf; minKey[0] = -intInf;
			}
			sum = 0;
		}

	}

	void update(int curD = -1){
		if(left){
			switch(dimension){
				case 3:
					minIt(minKey[2],left->minKey[2]);
					maxIt(maxKey[2],left->maxKey[2]);
				case 2:
					minIt(minKey[1],left->minKey[1]);
					maxIt(maxKey[1],left->maxKey[1]);

				case 1:
					minIt(minKey[0],left->minKey[0]);
					maxIt(maxKey[0],left->maxKey[0]);
			}

		}
		if(right){
			switch(dimension){
				case 3:
					minIt(minKey[2],right->minKey[2]);
					maxIt(maxKey[2],right->maxKey[2]);
				case 2:
					minIt(minKey[1],right->minKey[1]);
					maxIt(maxKey[1],right->maxKey[1]);

				case 1:
					minIt(minKey[0],right->minKey[0]);
					maxIt(maxKey[0],right->maxKey[0]);
			}
		}
		sum = (long long)(left?left->sum:0 )+ (long long )(right?right->sum:0 )+ (data?data->value:0);
		//	TODO: enable when needed
		//	nodeCount = left->nodeCount + right->nodeCount + 1;

	}
};





template < typename nodeType, typename keyType>
class kdTree{

	keyType q1[dimension];
	keyType q2[dimension];
	kdTreeNode<nodeType,keyType>* root;


	cacheControler< kdTreeNode<nodeType,keyType> >* nodeControler;
	int tmp;



	kdTreeNode<nodeType,keyType>*	 maketree(nodeType* nodelist,int curD,int n){

		if (n==0)
			return NULL;
		if (n==1){
			tmp++;
			kdTreeNode<nodeType,keyType> * cur = nodeControler->request();
			cur->init(NULL,NULL,nodelist);
			return cur;
		}
		int mid = n/2;
		selectKforKDTREE(nodelist,n,mid,curD);
		kdTreeNode<nodeType,keyType>* cur = nodeControler->request();
		int nextD = (curD+1==dimension)?0:curD+1;
		kdTreeNode<nodeType,keyType>* left = maketree(nodelist,nextD,mid);
		kdTreeNode<nodeType,keyType>* right = maketree(nodelist+mid,nextD,n-mid);
		cur->init(left,right,NULL,n);

		cur->update(curD);

		return cur;
	}
	
	bool contained(kdTreeNode<nodeType,keyType>* cur){
		switch(dimension){
			case 3: 
			if (!(q1[2]<=cur->minKey[2]&&cur->maxKey[2]<=q2[2]))
				return false;
			case 2:
			if (!(q1[1]<=cur->minKey[1]&&cur->maxKey[1]<=q2[1]))
				return false;
			case 1:
			if (!(q1[0]<=cur->minKey[0]&&cur->maxKey[0]<=q2[0]))
				return false;
		}

		return true;
	}
	bool intersected(kdTreeNode<nodeType,keyType>* cur){
		switch(dimension){
			case 3: 
			if (cur->maxKey[2]<q1[2]||q2[2]<cur->minKey[2])
				return false;
			case 2:
			if (cur->maxKey[1]<q1[1]||q2[1]<cur->minKey[1])
				return false;
			case 1:
			if (cur->maxKey[0]<q1[0]||q2[0]<cur->minKey[0])
				return false;
		}
		return true;
	}



	void query(kdTreeNode<nodeType,keyType>* cur,int curD,void(*integrate)(kdTreeNode<nodeType,keyType>*)){
		int nextD = curD+1==dimension?0:curD+1;

		if(cur->left==NULL&&cur->right==NULL){
			if(contained(cur))
				integrate(cur);
		}
		else{

			if (cur->left){
				if (contained(cur->left)){
					integrate(cur->left);
				}
				else if (intersected(cur->left)){
					query(cur->left,nextD,integrate);
				}
			}

			if (cur->right){
				if(contained(cur->right)){
					integrate(cur->right);
				}
				else if(intersected(cur->right)){
					query(cur->right,nextD,integrate);
				}
			}
		}
	}



	public:

	kdTree(int size){
		assert(dimension<=3);
		root = NULL;
		nodeControler = NULL;
		nodeControler = new cacheControler< kdTreeNode<nodeType,keyType> > (size);
		if(nodeControler==NULL){
			std::cerr<<"slot nodeControler error"<<endl;
		}
	}
	~kdTree(){
		if(root)
			root->trash(nodeControler);
		if(nodeControler)
			delete nodeControler;
	}
	void init(nodeType* nodelist,int Lsize){
		tmp = 0;
		if (nodelist)
			root = maketree(nodelist,0,Lsize);

	}
	void query(const nodeType& lowBound, const nodeType& highBound, void(*integrate) (kdTreeNode<nodeType,keyType>* )){
		for (int i = 0;i<dimension;i++){
			q1[i] = lowBound.keys[i];
			q2[i] = highBound.keys[i];
		}
		if(root)query(root,0,integrate);
	}

	//TODO: need to add
	void insert(const nodeType& newNode){}

};






#endif
