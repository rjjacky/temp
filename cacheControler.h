#ifndef __CACHECONTROLER_H__
#define __CACHECONTROLER_H__
#include "vec.h"
#include "constants.h"
#include <cstdlib>
static const int DEFAULT_SIZE = 3;
template<typename T>
class cacheControler{
private:
	T* _elem;
	int _capcity;
	int _size;
	vec<T*> trashCan;
public:
	cacheControler(int s = DEFAULT_SIZE){
		_elem = 0;
		_elem = new T[s];
		if(!_elem){
			exit(new_alot_error);
		}
		_size = 0;
		_capcity = s;
	}
	~cacheControler(){
		delete[] _elem;
	}
	T* request(){
		if(trashCan.size()){
			T* t = trashCan.back();
			trashCan.pop_back();
			return t;
		}
		else{
			return _elem+(_size++);
		}
	}

	void trash(T* t){
		trashCan.push_back(t);
	}

};

#endif
