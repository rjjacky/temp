#ifndef __MATH_RJ_H
#define __MATH_RJ_H

template<typename T>
inline void swap_rj(T& a , T& b){
	T x = a;
	a = b;
	b = x;
}

template<typename T>
inline void maxIt(T& a,const T&b){
	if(a<b)a=b;
}
template<typename T>
inline T getMax(const T&a ,const T&b){
	return a<b?b:a;
}

template<typename T>
inline void minIt(T&a,const T&b){
	if(b<a)a=b;
}

template<typename T>
inline T getMin(const T&a ,const T&b){
	return (a<b)?a:b;
}

#endif
